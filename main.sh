#!/bin/bash

hostname=$(grep -o VM.$ /etc/hosts)
for var in  ${hostname};
do
  ssh root@"$var" 'bash -s' < deploy_script
done

